# module_call

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) |  >=1.0.9 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >=4.36.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_create_bucket"></a> [create\_bucket](#module\_create\_bucket) | git::https://gitlab.com/terraform_can/module.git | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_deep_archive"></a> [deep\_archive](#input\_deep\_archive) | if an objecgt is not access after 180 days it will be moved to deep archive for cost efficiency. | `number` | n/a | yes |
| <a name="input_name_bucket"></a> [name\_bucket](#input\_name\_bucket) | the name of the S3 Bucket | `string` | n/a | yes |
| <a name="input_normal_archive"></a> [normal\_archive](#input\_normal\_archive) | if an objecgt is not access after 125 days it will be moved to archive for cost efficiency. | `number` | n/a | yes |
| <a name="input_o_lock_retention"></a> [o\_lock\_retention](#input\_o\_lock\_retention) | Set object lock retention as days. | `number` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
