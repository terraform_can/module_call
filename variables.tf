variable "name_bucket" {
  description = "the name of the S3 Bucket"
  type        = string
}
variable "o_lock_retention" {
  description = "Set object lock retention as days."
  type        = number
}
variable "deep_archive" {
  description = "if an objecgt is not access after 180 days it will be moved to deep archive for cost efficiency."
  type        = number
}
variable "normal_archive" {
  description = "if an objecgt is not access after 125 days it will be moved to archive for cost efficiency."
  type        = number
}