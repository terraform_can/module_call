module "create_bucket" {
  #source       = "../module/"
  source                     = "git::https://gitlab.com/terraform_can/module.git"
  bucket_name                = var.name_bucket
  object_lock_retention_days = var.o_lock_retention
  deep_archive_days          = var.deep_archive
  archive_days               = var.normal_archive
}